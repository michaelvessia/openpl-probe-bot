package parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import models.Federation;
import models.Meet;

public class Parser {

  private static Logger logger = Logger.getLogger(Parser.class.getName());

  /**
   * Given a line of the probe output, return the URL of the meet as a string
   */
  public Function<String, String> extractUrl = (line) -> {
    Pattern regex = Pattern.compile(".*(http.*$)");
    Matcher matcher = regex.matcher(line);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return "";
  };

  public Function<String, String> extractFed = (line) -> {
    String sanitizedLine = line.replaceAll("\\[1;", "").replaceAll("\\[0;", "");
    Pattern regex = Pattern.compile(".*\\[(.*)\\].*");
    Matcher matcher = regex.matcher(sanitizedLine);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return "";
  };

  public Function<String, Meet> toMeet = (line) -> {
    Meet meet = new Meet();
    meet.setFederationName(extractFed.apply(line));
    meet.setMeetUrl(extractUrl.apply(line));
    return meet;
  };

  /**
   * Given a line, return a Predicate based on whether or not it contains a url
   */
  public Predicate<String> hasUrl = (line) -> {
    return line.contains("http");
  };

  /**
   * Given stream of lines, return list of federations
   * 
   * @param lines
   * @return
   */
  public List<Federation> buildFedList(Stream<String> lineStream) {
    // Group the meets by federation name in map and close the original stream
    Map<String, List<Meet>> meetsByFedName =
        lineStream.map(toMeet).collect(Collectors.groupingBy(Meet::getFederationName));
    lineStream.close();

    // Create a list of Federation objects from the map
    List<Federation> feds = meetsByFedName.entrySet().stream()
        .map(item -> new Federation(item.getKey(), item.getValue())).collect(Collectors.toList());

    return feds;
  }

  /**
   * Get a stream containing all of the lines that contain meet urls in the probe output
   * 
   * Consumer needs to close the stream
   * 
   * @param inputFilePath
   * @return
   */
  public Stream<String> processInputFile(String inputFilePath) {

    Stream<String> meetLines = null;
    try {
      File inputF = new File(inputFilePath);
      InputStream inputFS = new FileInputStream(inputF);
      BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
      meetLines = br.lines().filter(hasUrl).onClose(() -> {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
    } catch (IOException e) {
      logger.log(Level.SEVERE, "Error reading in probe output");
      e.printStackTrace();
    }
    return meetLines;
  }
}

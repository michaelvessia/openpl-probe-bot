package requests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import models.Federation;
import parser.Parser;

public class ZulipService {

  private static Logger logger = Logger.getLogger(ZulipService.class.getName());

  public static final String API_URL_COMPONENT = "/api/v1/messages";

  private String url;
  private String username;
  private String password;

  public ZulipService(String url, String username, String password) {
    this.url = url;
    this.username = username;
    this.password = password;
  }

  /**
   * Create the message, which is a list of pairs for http headers
   * 
   * @param type
   * @param to
   * @param subject
   * @param content
   * @return
   */
  private List<NameValuePair> createMessage(String type, String to, String subject,
      String content) {
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    params.add(new BasicNameValuePair("type", type));
    params.add(new BasicNameValuePair("to", to));
    params.add(new BasicNameValuePair("subject", subject));
    params.add(new BasicNameValuePair("content", content));
    return params;
  }

  private UsernamePasswordCredentials getCreds() {
    UsernamePasswordCredentials creds =
        new UsernamePasswordCredentials(this.username, this.password);
    return creds;
  }

  public void postMeetsToZulip(String inputFile, String stream) {
    Parser parser = new Parser();
    List<Federation> feds = parser.buildFedList(parser.processInputFile(inputFile));

    for (Federation fed : feds) {
      try {
        postMessage("stream", stream, fed.getName(), fed.getMessage());
      } catch (AuthenticationException | IOException e) {
        logger.log(Level.SEVERE, "Could not send meets for federation " + fed.getName(), e);
        e.printStackTrace();
      }
    }
  }

  public CloseableHttpResponse postMessage(String type, String to, String subject, String content)
      throws ClientProtocolException, IOException, AuthenticationException {
    // Open the http client, pointing to the configured zulipchat url
    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost(url);
    // Set up the request params which will define the message to be sent
    List<NameValuePair> message = createMessage(type, to, subject, content);
    httpPost.setEntity(new UrlEncodedFormEntity(message));
    // Add the headers to the request, including authorization
    httpPost.addHeader(new BasicScheme().authenticate(getCreds(), httpPost, null));

    // Get the response and close the http client
    CloseableHttpResponse response = client.execute(httpPost);
    client.close();
    return response;
  }
}

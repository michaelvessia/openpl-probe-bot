package main;

import requests.ZulipService;

public class Main {

  public static void main(String[] args) {
    String baseUrl = System.getenv("ZULIP_BASE_URL");
    String username = System.getenv("ZULIP_PROBEBOT_EMAIL");
    String password = System.getenv("ZULIP_PROBEBOT_API_KEY");
    String url = baseUrl + ZulipService.API_URL_COMPONENT;
    String inputFile = "src/main/resources/sample_output.txt";
    String stream = System.getenv("ZULIP_PROBEBOT_STREAM");

    ZulipService service = new ZulipService(url, username, password);
    service.postMeetsToZulip(inputFile, stream);
  }
}

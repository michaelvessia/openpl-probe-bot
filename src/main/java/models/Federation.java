package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

public class Federation {

  public static BiFunction<String, List<Meet>, Federation> getFederation = (name, meets) -> {
    return new Federation(name, meets);
  };
  private String name;

  private List<Meet> meets = new ArrayList<>();

  public Federation() {}

  public Federation(String name, List<Meet> meets) {
    super();
    this.name = name;
    this.meets = meets;
  }

  public List<Meet> getMeets() {
    return meets;
  }

  public String getMessage() {
    StringBuilder message = new StringBuilder();
    message.append("Meet count: ");
    message.append(meets.size());
    message.append("\n");

    Optional<String> opt = meets.stream().map(meet -> meet.getMeetUrl()).reduce((m1, m2) -> m1 + "\n" + m2);
    opt.ifPresent(anyMeets -> message.append(anyMeets));
    return message.toString();
  }

  public String getName() {
    return name;
  }

  public void setMeets(List<Meet> meets) {
    this.meets = meets;
  }

  public void setName(String name) {
    this.name = name;
  }
}

package models;

public class Meet {

  private String federationName;
  private String meetUrl;

  public String getFederationName() {
    return federationName;
  }

  public String getMeetUrl() {
    return meetUrl;
  }

  public void setFederationName(String federationName) {
    this.federationName = federationName;
  }

  public void setMeetUrl(String meetUrl) {
    this.meetUrl = meetUrl;
  }

}

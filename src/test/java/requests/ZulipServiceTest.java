package requests;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.junit.BeforeClass;
import org.junit.Test;

public class ZulipServiceTest {

  private static ZulipService zulipService;

  @BeforeClass
  public static void createService() {
    String baseUrl = System.getenv("ZULIP_BASE_URL");
    String username = System.getenv("ZULIP_PROBEBOT_EMAIL");
    String password = System.getenv("ZULIP_PROBEBOT_API_KEY");
    String url = baseUrl + ZulipService.API_URL_COMPONENT;
    zulipService = new ZulipService(url, username, password);
  }

  @Test
  public void postMessageTest()
      throws AuthenticationException, ClientProtocolException, IOException {
    // Not sure we really need to test the api connectivity, but it might be useful for developer setup
    //CloseableHttpResponse response = zulipService.postMessage("stream", "general", "test-subject", "test-content");
    //assertEquals(200, response.getStatusLine().getStatusCode());
  }
}

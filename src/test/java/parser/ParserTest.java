package parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;
import models.Federation;
import models.Meet;

public class ParserTest {

  private static Parser parser;
  private static String probeOutputPath = "src/test/resources/sample_output.txt";
  private static String exampleLine = "[1;35m[AAU][0;m http://image.aausports.org/dnn/strengthsports/2017/Results/2017NorthAmericanResults.pdf";


  @BeforeClass
  public static void instantiateParser() {
    parser = new Parser();
  }

  @Test
  public void extractUrlTest() {
    String url = parser.extractUrl.apply(exampleLine);
    assertEquals(
        "http://image.aausports.org/dnn/strengthsports/2017/Results/2017NorthAmericanResults.pdf",
        url);
  }

  @Test
  public void extractFedTest() {
    String fed = parser.extractFed.apply(exampleLine);
    assertEquals("AAU", fed);
  }
  
  @Test
  public void hasUrlTest() {
    String pass1 = exampleLine;
    String pass2 = "https://www.example.com";
    String fail1 = "I don't contain a link";
    String fail2 = "I contain the letters h,t,t,p, but no link";
    
    assertTrue(parser.hasUrl.test(pass1));
    assertTrue(parser.hasUrl.test(pass2));

    assertFalse(parser.hasUrl.test(fail1));
    assertFalse(parser.hasUrl.test(fail2));

  }
  
  @Test
  public void toMeet() {
    Meet meet = new Meet();
    meet.setFederationName("AAU");
    meet.setMeetUrl("http://image.aausports.org/dnn/strengthsports/2017/Results/2017NorthAmericanResults.pdf");
    
    Meet test = parser.toMeet.apply(exampleLine);
    
    assertEquals(meet.getFederationName(), test.getFederationName());
    assertEquals(meet.getMeetUrl(), test.getMeetUrl());
  }

  @Test
  public void processInputFileTest() {
    Stream<String> stream = parser.processInputFile(probeOutputPath);
    assertNotNull(stream);
  }
  
  @Test
  public void buildFedListTest() {
    Stream<String> stream = parser.processInputFile(probeOutputPath);

    List<Federation> result = parser.buildFedList(stream);
    List<String> fedNames = result.stream().map(Federation::getName).collect(Collectors.toList());
    assertEquals(3, result.size());
    assertThat(fedNames, CoreMatchers.hasItems("AAU", "FPO", "FFForce"));
  }

}
